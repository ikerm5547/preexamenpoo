/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelado.de.clases;

/**
 *
 * @author Iker Martinez
 */
public class pagorecibo {
    int numContrato;
    private String nomEmpleado;
    private String dom;
    private int tipoContrato;
    private int nivelEstudio;
    private float pagoDiario;
    private int diasTrabajados;
    
    public pagorecibo() {
        this.numContrato = 102;
        this.nomEmpleado = "jose lopez";
        this.dom = "Av del sol 1200";
        this.tipoContrato = 1;
        this.nivelEstudio = 1;
        this.pagoDiario = 700.00f;
        this.diasTrabajados = 15;
    }

    public pagorecibo(int numContrato, String nomEmpleado, String dom, int tipoContrato, int nivelEstudio, float pagoDiario, int diasTrabajados) {
        this.numContrato = numContrato;
        this.nomEmpleado = nomEmpleado;
        this.dom = dom;
        this.tipoContrato = tipoContrato;
        this.nivelEstudio = nivelEstudio;
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }
  
    public pagorecibo(pagorecibo otro){
    this.numContrato = otro.numContrato;
    this.nomEmpleado = otro.nomEmpleado;
    this.dom = otro.dom;
    this.tipoContrato = otro.tipoContrato;
    this.nivelEstudio = otro.nivelEstudio;
    this.pagoDiario = otro.pagoDiario;
    this.diasTrabajados = otro.diasTrabajados;
    }

    public int getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(int numContrato) {
        this.numContrato = numContrato;
    }

    public String getNomEmpleado() {
        return nomEmpleado;
    }

    public void setNomEmpleado(String nomEmpleado) {
        this.nomEmpleado = nomEmpleado;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public int getNivelEstudio() {
        return nivelEstudio;
    }

    public void setNivelEstudio(int nivelEstudio) {
        this.nivelEstudio = nivelEstudio;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    
    
    
    
    
    
    
    
    public float calcularSubtotal() {
        // Definimos los incrementos según el nivel de estudios
        float subtotal = 0.0f;
        float estudio = 0.0f;;
        if (this.nivelEstudio == 1){
        subtotal = this.diasTrabajados * (this.pagoDiario + this.pagoDiario  * 0.20f);}
        if (this.nivelEstudio == 2){
        subtotal = this.diasTrabajados * (this.pagoDiario + this.pagoDiario  * 0.50f);}
        if (this.nivelEstudio == 3){
        subtotal = this.diasTrabajados * (this.pagoDiario + this.pagoDiario);}
        
        return subtotal;                  
    }
    
    public float calcularImpuesto(){
    float impuesto = 0.0f;
    float sub = calcularSubtotal();
    impuesto = sub * 0.16f;
    return impuesto;
    }
    
    public float obtenerTotalPagar(){
    float total = 0.0f;
    float sub = calcularSubtotal();
    float impuesto = calcularImpuesto();
    total = sub - impuesto;
    return total;
    }
}
