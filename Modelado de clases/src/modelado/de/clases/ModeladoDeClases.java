/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelado.de.clases;

/**
 *
 * @author Iker Martinez
 */
public class ModeladoDeClases {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        pagorecibo pago1 = new pagorecibo();
        
        System.out.println("Nombre de empleado: " + pago1.getNomEmpleado()); 
        System.out.println("Domicilio: " + pago1.getDom());
        System.out.println("Tipo de contrato: " + pago1.getTipoContrato());
        System.out.println("Nivel de estudio: " + pago1.getNivelEstudio());
        System.out.println("Pago diario base: " + pago1.getPagoDiario());
        System.out.println("Dias trabajado:" + pago1.getDiasTrabajados());
        System.out.println("SUBTOTAL: " + pago1.calcularSubtotal());
        System.out.println("IMPUESTO: " + pago1.calcularImpuesto());
        System.out.println("TOTAL PAGAR: " + pago1.obtenerTotalPagar());
System.out.println("------------------------------------------------------------------------------");
        pagorecibo2 pago2 = new pagorecibo2();
        System.out.println("Nombre de empleado: " + pago2.getNomEmpleado()); 
        System.out.println("Domicilio: " + pago2.getDom());
        System.out.println("Tipo de contrato: " + pago2.getTipoContrato());
        System.out.println("Nivel de estudio: " + pago2.getNivelEstudio());
        System.out.println("Pago diario base: " + pago2.getPagoDiario());
        System.out.println("Dias trabajado:" + pago2.getDiasTrabajados());
        System.out.println("SUBTOTAL: " + pago2.calcularSubtotal());
        System.out.println("IMPUESTO: " + pago2.calcularImpuesto());
        System.out.println("TOTAL PAGAR: " + pago2.obtenerTotalPagar());
        
    }
}
